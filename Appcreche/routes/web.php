<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Creches
    Route::delete('creches/destroy', 'CrechesController@massDestroy')->name('creches.massDestroy');
    Route::resource('creches', 'CrechesController');

    // Bebes
    Route::delete('bebes/destroy', 'BebesController@massDestroy')->name('bebes.massDestroy');
    Route::resource('bebes', 'BebesController');

    // Informations_supplementaires
    Route::delete('informations_supplementaires/destroy', 'Informations_supplementairesController@massDestroy')->name('informations_supplementaires.massDestroy');
    Route::resource('informations_supplementaires', 'Informations_supplementairesController');
});
