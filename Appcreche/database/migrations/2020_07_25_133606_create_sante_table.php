<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sante', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->string('taille');
			$table->string('poids');
			$table->string('maturite');
			$table->string('enfant_parle');
			$table->date('date_parler');
			$table->string('biberion');
			$table->string('allaitement');
			$table->string('enfant_marche');
			$table->date('date_marche');
			$table->string('tetine');
			$table->string('manger_seul');
			$table->string('manger_bien');
			$table->string('aliments_preferes');
			$table->string('habitude_dormir');
			$table->string('jouer');
			$table->string('autre_creche');
			$table->string('raison_sortie');
			$table->string('maladies');
			$table->string('allergie_alimentaire');
			$table->string('allergie_medicamenteuse');
			$table->string('details');
			$table->string('autre_difficulte');
			$table->string('precaution_difficulte');
			$table->string('autre_recommandation');
			$table->integer('bebe_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sante');
    }
}
