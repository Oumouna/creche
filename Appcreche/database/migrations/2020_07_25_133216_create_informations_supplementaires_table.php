<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationsSupplementairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations_supplementaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('situation_familiale');
			$table->string('responsable_legal');
			$table->string('nom_persprev');
			$table->string('prenom_persprev');
			$table->string('telephone_persprev');
			$table->string('lien_persprev');
			$table->bigInteger('cni_persprev');
			$table->string('nom_pediatre');
			$table->string('prenom_pediatre');
			$table->mediumInteger('telephone_pediatre');
			$table->string('nom_persautorisee');
			$table->string('prenom_persautorisee');
			$table->mediumInteger('telephone_persautorisee');
            $table->integer('bebe_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations_supplementaires');
    }
}
