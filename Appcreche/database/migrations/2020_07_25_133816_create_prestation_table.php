<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestation', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->string('garde_continue');
			$table->date('date_debut_garde');
			$table->date('date_fin_garde');
			$table->string('cantine_continue');
			$table->string('date_debut_cantine');
			$table->string('date_fin_cantine');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestation');
    }
}
