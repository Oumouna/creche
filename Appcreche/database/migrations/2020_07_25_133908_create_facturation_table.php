<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturation', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->date('mois');
			$table->bigInteger('montant_garde_continue');
			$table->bigInteger('montant_garde_ponctuel');
			$table->bigInteger('montant_cantine_continue');
			$table->bigInteger('montant_cantine_ponctuel');
			$table->bigInteger('montant_global');
			$table->bigInteger('facture_payer');
			$table->string('mode_paiement_facture');
			$table->date('date_paiement_facture');
			$table->integer('prestation_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturation');
    }
}
