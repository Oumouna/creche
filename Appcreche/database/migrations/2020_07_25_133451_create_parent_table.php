<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->string('nom', 255);
			$table->string('prenom', 255);
			$table->string('adresse');
			$table->string('profession');
			$table->mediumInteger('telephone1');
			$table->mediumInteger('telephone2');
			$table->mediumInteger('numero_whatsapp');
			$table->string('email1');
			$table->string('email2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent');
    }
}
