<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneeBebeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journee_bebe', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->date('arrivee');
			$table->date('depart');
			$table->string('temperature');
			$table->string('humeur');
			$table->date('heure_petitdej');
			$table->string('comment_petitdej');
			$table->date('heure_dej');
			$table->string('comment_dej');
			$table->date('heure_dessert');
			$table->string('comment_dessert');
			$table->date('heure_gouter');
			$table->string('comment_gouter');
			$table->smallInteger('nombre_selle');
			$table->string('comment_selle');
			$table->date('sommeil1');
			$table->date('sommeil2');
			$table->date('sommeil3');
			$table->date('sommeil4');
			$table->date('sommeil5');
			$table->string('medoc_remarque');
			$table->integer('bebe_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journee_bebe');
    }
}
