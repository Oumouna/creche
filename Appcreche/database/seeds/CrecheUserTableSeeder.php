<?php

use App\User;
use Illuminate\Database\Seeder;

class CrecheUserTableSeeder extends Seeder
{
    public function run()
    {
        User::findOrFail(1)->creches()->sync(1);
    }
}
