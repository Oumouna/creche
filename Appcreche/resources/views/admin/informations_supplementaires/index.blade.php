@extends('layouts.admin')
@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.bebes.create") }}">
                {{ trans('global.add') }} {{ trans('bébé') }}
            </a>
        </div>
    </div>
<div class="card">
    <div class="card-header">
        {{ trans('bébé') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-bebe">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('id') }}
                        </th>
                        <th>
                            {{ trans('nom') }}
                        </th>
                        <th>
                            {{ trans('prenom') }}
                        </th>
                        <th>
                            {{ trans('date de naissance') }}
                        </th>
                        <th>
                            {{ trans('sexe') }}
                        </th>
                        <th>
                            {{ trans('nationalité') }}
                        </th>
                        <th>
                            {{ trans('Crèche') }}
                        </th>
                        <th>
                            
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bebes as $key => $bebe)
                        <tr data-entry-id="{{ $bebe->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $bebe->id ?? '' }}
                            </td>
                            <td>
                                {{ $bebe->nom ?? '' }}
                            </td>
                            <td>
                                {{ $bebe->prenom ?? '' }}
                            </td>
                            <td>
                                {{ $bebe->date_naissance ?? '' }}
                            </td>
                            <td>
                                {{ $bebe->sexe ?? '' }}
                            </td>
                            <td>
                                {{ $bebe->nationalite ?? '' }}
                            </td>
                            <td>
                                <span class="badge badge-info">{{ $bebe->creche->nom ?? '' }}</span>
                            </td>
                            <td>
                                
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.bebes.show', $bebe->id) }}">
                                        {{ trans('Info Sup') }}
                                    </a>
                              

                                
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.bebes.edit', $bebe->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                              

                                
                                    <form action="{{ route('admin.bebes.destroy', $bebe->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                              
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection

