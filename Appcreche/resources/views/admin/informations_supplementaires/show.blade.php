@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('bébé') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bebes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('Id') }}
                        </th>
                        <td>
                            {{ $bebe->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Nom') }}
                        </th>
                        <td>
                            {{ $bebe->nom }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Prénom') }}
                        </th>
                        <td>
                            {{ $bebe->prenom }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Date de naissance') }}
                        </th>
                        <td>
                            {{ $bebe->date_naissance }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Sexe') }}
                        </th>
                        <td>
                            {{ $bebe->sexe }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Nationalité') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Crèche') }}
                        </th>
                        <td>
                            <span class="badge badge-info">{{ $bebe->creche->nom }}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bebes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection