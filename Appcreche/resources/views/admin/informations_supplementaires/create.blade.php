@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('Informations sup') }}
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link" style="color:black;" id="infosup-tab" data-toggle="tab" href="#infosup" role="tab" aria-controls="infosup" aria-selected="false">Informations supplémentaires</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="color:black;" id="parent-tab" data-toggle="tab" href="#parent" role="tab" aria-controls="parent" aria-selected="false">Parents</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="color:black;" id="sante-tab" data-toggle="tab" href="#sante" role="tab" aria-controls="sante" aria-selected="false">Santé</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="color:black;" id="prestation-tab" data-toggle="tab" href="#prestation" role="tab" aria-controls="prestation" aria-selected="false">Prestation</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade" id="infosup" role="tabpanel" aria-labelledby="infosup-tab">
                <br>
                <label class="required" for="situation_familiale">{{ trans('Situation familiale') }}</label>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio1" value="Célibataire" checked>
                    <label class="form-check-label" for="inlineRadio1">Célibataire</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio2" value="Marié(e)">
                    <label class="form-check-label" for="inlineRadio2">Marié(e)</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio3" value="Divorcé(e)">
                    <label class="form-check-label" for="inlineRadio3">Divorcé(e)</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio3" value="Séparé(e)">
                    <label class="form-check-label" for="inlineRadio3">Séparé(e)</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio3" value="Veuf(ve">
                    <label class="form-check-label" for="inlineRadio3">Veuf(ve)</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="situation_familiale" id="inlineRadio3" value="Concubinage">
                    <label class="form-check-label" for="inlineRadio3">Concubinage</label>
                </div>
                <br><br>
                <div class="form-group">
                    <label class="required" for="responsable_legal">{{ trans('Responsable légal(e)') }}</label>
                    <br>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="responsable_legal" id="inlineRadio1" value="Père" checked>
                    <label class="form-check-label" for="inlineRadio1">Père</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="responsable_legal" id="inlineRadio2" value="Mère">
                    <label class="form-check-label" for="inlineRadio2">Mère</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="responsable_legal" id="inlineRadio3" value="Tuteur">
                    <label class="form-check-label" for="inlineRadio3">Tuteur</label>
                </div>
                </div>
                <Label><i> Personne à prévenir en cas d'urgence</i></Label>
                <br>
                <div class="form-row">
                        <div class="col">
                                <label class="required" for="nom_persprev">{{ trans('Nom') }}</label>
                                <input class="form-control {{ $errors->has('nom_persprev') ? 'is-invalid' : '' }}" type="text" name="nom_persprev" id="nom_persprev" value="{{ old('nom_persprev', '') }}" required>
                                @if($errors->has('nom_persprev'))
                                    <span class="text-danger">{{ $errors->first('nom_persprev') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>
                        <div class="col">
                                <label class="required" for="prenom_persprev">{{ trans('Prénom') }}</label>
                                <input class="form-control {{ $errors->has('prenom_persprev') ? 'is-invalid' : '' }}" type="text" name="prenom_persprev" id="prenom_persprev" value="{{ old('prenom_persprev', '') }}" required>
                                @if($errors->has('prenom_persprev'))
                                    <span class="text-danger">{{ $errors->first('prenom_persprev') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>      
                </div>
                <br>
                <div class="form-row">
                        <div class="col">
                                <label class="required" for="telephone_persprev">{{ trans('Téléphone') }}</label>
                                <input class="form-control {{ $errors->has('telephone_persprev') ? 'is-invalid' : '' }}" type="number" name="telephone_persprev" id="telephone_persprev" value="{{ old('telephone_persprev', '') }}" required>
                                @if($errors->has('telephone_persprev'))
                                    <span class="text-danger">{{ $errors->first('telephone_persprev') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>
                        <div class="col">
                                <label class="required" for="cni_persprev">{{ trans('CNI') }}</label>
                                <input class="form-control {{ $errors->has('cni_persprev') ? 'is-invalid' : '' }}" type="number" name="cni_persprev" id="cni_persprev" value="{{ old('cni_persprev', '') }}" required>
                                @if($errors->has('cni_persprev'))
                                    <span class="text-danger">{{ $errors->first('cni_persprev') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>      
                </div>
                <br> 
                <div class="form-group">
                    <label class="required" for="lien_persprev">{{ trans('Lien') }}</label>
                        <input class="form-control {{ $errors->has('lien_persprev') ? 'is-invalid' : '' }}" type="text" name="lien_persprev" id="lien_persprev" value="{{ old('lien_persprev', '') }}" required>
                        @if($errors->has('lien_persprev'))
                            <span class="text-danger">{{ $errors->first('lien_persprev') }}</span>
                        @endif
                        <span class="help-block">{{ trans('') }}</span>
                </div>
                <Label><i> Pédiatre de l'enfant</i></Label>
                <br>
                <div class="form-row">
                        <div class="col">
                                <label class="required" for="nom_pediatre">{{ trans('Nom') }}</label>
                                <input class="form-control {{ $errors->has('nom_pediatre') ? 'is-invalid' : '' }}" type="text" name="nom_pediatre" id="nom_pediatre" value="{{ old('nom_pediatre', '') }}" required>
                                @if($errors->has('nom_pediatre'))
                                    <span class="text-danger">{{ $errors->first('nom_pediatre') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>
                        <div class="col">
                                <label class="required" for="prenom_pediatre">{{ trans('Prénom') }}</label>
                                <input class="form-control {{ $errors->has('prenom_pediatre') ? 'is-invalid' : '' }}" type="text" name="prenom_pediatre" id="prenom_pediatre" value="{{ old('prenom_pediatre', '') }}" required>
                                @if($errors->has('prenom_pediatre'))
                                    <span class="text-danger">{{ $errors->first('prenom_pediatre') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>      
                </div>
                <br>
                <div class="form-group">
                    <label class="required" for="telephone_pediatre">{{ trans('Téléphone') }}</label>
                    <input class="form-control {{ $errors->has('telephone_pediatre') ? 'is-invalid' : '' }}" type="number" name="telephone_pediatre" id="telephone_pediatre" value="{{ old('telephone_pediatre', '') }}" required>
                        @if($errors->has('telephone_pediatre'))
                            <span class="text-danger">{{ $errors->first('telephone_pediatre') }}</span>
                        @endif
                        <span class="help-block">{{ trans('') }}</span>
                </div>
                <Label><i> Personne autorisée à venir prendre l'enfant</i></Label>
                <div class="form-row">
                        <div class="col">
                            <label class="required" for="nom_persautorisee">{{ trans('Nom') }}</label>
                            <input class="form-control {{ $errors->has('nom_persautorisee') ? 'is-invalid' : '' }}" type="text" name="nom_persautorisee" id="nom_persautorisee" value="{{ old('nom_persautorisee', '') }}" required>
                                @if($errors->has('nom_persautorisee'))
                                    <span class="text-danger">{{ $errors->first('nom_persautorisee') }}</span>
                                @endif
                            <span class="help-block">{{ trans('') }}</span>
                        </div>
                        <div class="col">
                                <label class="required" for="prenom_persautorisee">{{ trans('Prénom') }}</label>
                                <input class="form-control {{ $errors->has('prenom_persautorisee') ? 'is-invalid' : '' }}" type="text" name="prenom_persautorisee" id="prenom_persautorisee" value="{{ old('prenom_persautorisee', '') }}" required>
                                @if($errors->has('prenom_persautorisee'))
                                    <span class="text-danger">{{ $errors->first('prenom_persautorisee') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>      
                </div>
                <br>
                <div class="form-group">
                    <label class="required" for="telephone_persautorisee">{{ trans('Téléphone') }}</label>
                    <input class="form-control {{ $errors->has('telephone_persautorisee') ? 'is-invalid' : '' }}" type="number" name="telephone_persautorisee" id="telephone_persautorisee" value="{{ old('telephone_persautorisee', '') }}" required>
                        @if($errors->has('telephone_persautorisee'))
                            <span class="text-danger">{{ $errors->first('telephone_persautorisee') }}</span>
                        @endif
                        <span class="help-block">{{ trans('') }}</span>
                </div>
        </div> <br><br>


    <div class="tab-pane fade" id="parent" role="tabpanel" aria-labelledby="parent-tab">
            <br>
            <div class="form-row">
                    <div class="col">
                            <label class="required" for="nom">{{ trans('Nom') }}</label>
                            <input class="form-control {{ $errors->has('nom') ? 'is-invalid' : '' }}" type="text" name="nom" id="nom" value="{{ old('nom', '') }}" required>
                            @if($errors->has('nom'))
                                <span class="text-danger">{{ $errors->first('nom') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>
                    <div class="col">
                            <label class="required" for="prenom">{{ trans('Prénom') }}</label>
                            <input class="form-control {{ $errors->has('prenom') ? 'is-invalid' : '' }}" type="text" name="prenom" id="prenom" value="{{ old('prenom', '') }}" required>
                            @if($errors->has('prenom'))
                                <span class="text-danger">{{ $errors->first('prenom') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>      
            </div>
            <br>
            <div class="form-row">
                    <div class="col">
                            <label class="required" for="adresse">{{ trans('Adresse') }}</label>
                            <input class="form-control {{ $errors->has('adresse') ? 'is-invalid' : '' }}" type="text" name="adresse" id="adresse" value="{{ old('adresse', '') }}" required>
                            @if($errors->has('adresse'))
                                <span class="text-danger">{{ $errors->first('adresse') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>
                    <div class="col">
                            <label class="required" for="profession">{{ trans('Profession') }}</label>
                            <input class="form-control {{ $errors->has('profession') ? 'is-invalid' : '' }}" type="text" name="profession" id="profession" value="{{ old('profession', '') }}" required>
                            @if($errors->has('profession'))
                                <span class="text-danger">{{ $errors->first('profession') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>      
            </div>
            <br>
            <div class="form-row">
                    <div class="col">
                            <label class="required" for="telephone1">{{ trans('Téléphone 1') }}</label>
                            <input class="form-control {{ $errors->has('telephone1') ? 'is-invalid' : '' }}" type="text" name="telephone1" id="telephone1" value="{{ old('telephone1', '') }}" required>
                            @if($errors->has('telephone1'))
                                <span class="text-danger">{{ $errors->first('telephone1') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>
                    <div class="col">
                            <label class="required" for="telephone2">{{ trans('Téléphone 2') }}</label>
                            <input class="form-control {{ $errors->has('telephone2') ? 'is-invalid' : '' }}" type="text" name="telephone2" id="telephone2" value="{{ old('telephone2', '') }}" required>
                            @if($errors->has('telephone2'))
                                <span class="text-danger">{{ $errors->first('telephone2') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>
                    <div class="col">
                            <label class="required" for="numero_whatsapp">{{ trans('Numero whatsapp') }}</label>
                            <input class="form-control {{ $errors->has('numero_whatsapp') ? 'is-invalid' : '' }}" type="text" name="numero_whatsapp" id="numero_whatsapp" value="{{ old('numero_whatsapp', '') }}" required>
                            @if($errors->has('numero_whatsapp'))
                                <span class="text-danger">{{ $errors->first('numero_whatsapp') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>      
            </div>
            <br>
            <div class="form-row">
                    <div class="col">
                            <label class="required" for="email1">{{ trans(' Email 1') }}</label>
                            <input class="form-control {{ $errors->has('email1') ? 'is-invalid' : '' }}" type="text" name="email1" id="email1" value="{{ old('email1', '') }}" required>
                            @if($errors->has('email1'))
                                <span class="text-danger">{{ $errors->first('email1') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>
                    <div class="col">
                            <label class="required" for="email2">{{ trans('Email 2') }}</label>
                            <input class="form-control {{ $errors->has('email2') ? 'is-invalid' : '' }}" type="text" name="email2" id="email2" value="{{ old('email2', '') }}" required>
                            @if($errors->has('email2'))
                                <span class="text-danger">{{ $errors->first('email2') }}</span>
                            @endif
                            <span class="help-block">{{ trans('') }}</span>
                    </div>      
            </div>
            <br><br><br>
    </div>



    <div class="tab-pane fade" id="sante" role="tabpanel" aria-labelledby="sante-tab">
                <Label><i> Enfant</i></Label>
                <div class="form-row">
                        <div class="col">
                                <label class="required" for="taille">{{ trans('Taille') }}</label>
                                <input class="form-control {{ $errors->has('taille') ? 'is-invalid' : '' }}" type="text" name="taille" id="taille" value="{{ old('taille', '') }}" required>
                                @if($errors->has('taille'))
                                    <span class="text-danger">{{ $errors->first('taille') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>
                        <div class="col">
                                <label class="required" for="poids">{{ trans('Poids') }}</label>
                                <input class="form-control {{ $errors->has('poids') ? 'is-invalid' : '' }}" type="text" name="poids" id="poids" value="{{ old('poids', '') }}" required>
                                @if($errors->has('poids'))
                                    <span class="text-danger">{{ $errors->first('poids') }}</span>
                                @endif
                                <span class="help-block">{{ trans('') }}</span>
                        </div>      
                </div>
                <br>
                <div class="form-group">
                        <label class="required" for="maturite">{{ trans('L\'enfant est-il né à terme ?') }}</label>
                        <br>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="maturite" id="inlineRadio1" value="oui" checked>
                        <label class="form-check-label" for="inlineRadio1">Oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="maturite" id="inlineRadio2" value="non">
                        <label class="form-check-label" for="inlineRadio2">Non</label>
                    </div>
                </div>
        </div>




        <div class="tab-pane fade" id="prestation" role="tabpanel" aria-labelledby="prestation-tab">prestation</div>
</div>

</div>
    <script>
        $('#myTab a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
        })
    </script>
@endsection