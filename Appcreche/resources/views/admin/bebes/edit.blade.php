@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('bébé') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.bebes.update", [$bebe->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="nom">{{ trans('Nom ') }}</label>
                <input class="form-control {{ $errors->has('nom') ? 'is-invalid' : '' }}" type="text" name="nom" id="nom" value="{{ old('nom', $bebe->nom) }}" required>
                @if($errors->has('nom'))
                    <span class="text-danger">{{ $errors->first('nom') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="prenom">{{ trans('Prénom') }}</label>
                <input class="form-control {{ $errors->has('prenom') ? 'is-invalid' : '' }}" type="text" name="prenom" id="prenom" value="{{ old('prenom', $bebe->prenom) }}" required>
                @if($errors->has('prenom'))
                    <span class="text-danger">{{ $errors->first('prenom') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_naissance">{{ trans('Date de naissance') }}</label>
                <input class="form-control {{ $errors->has('date_naissance') ? 'is-invalid' : '' }}" type="date" name="date_naissance" id="date_naissance" value="{{ old('date_naissance', $bebe->date_naissance) }}" required>
                @if($errors->has('date_naissance'))
                    <span class="text-danger">{{ $errors->first('date_naissance') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sexe">{{ trans('Sexe') }}</label>
                <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label class="form-check-label" for="sexe1" >
                <input class="form-check-input" class="form-control {{ $errors->has('sexe') ? 'is-invalid' : '' }}" type="radio" name="sexe" id="sexe1" value="masculin" {{ $bebe->sexe == 'masculin' ? 'checked' : '' }}> 
                Masculin
                </label>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <label class="form-check-label" for="sExe2" >
                <input class="form-check-input" class="form-control {{ $errors->has('sexe') ? 'is-invalid' : '' }}" type="radio" name="sexe" id="sexe2" value="feminin" {{ $bebe->sexe == 'feminin' ? 'checked' : '' }}>
                Féminin
            </label>
            </div>
            <div class="form-group">
                <label class="required" for="nationalite">{{ trans('Nationalité') }}</label>
                <input class="form-control {{ $errors->has('nationalite') ? 'is-invalid' : '' }}" type="text" name="nationalite" id="nationalite" value="{{ old('nationalite', $bebe->nationalite) }}" required>
                @if($errors->has('nationalite'))
                    <span class="text-danger">{{ $errors->first('nationalite') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label class="required" for="creches">{{ trans('creches') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 " name="creche_id" id="creche_id" multiple required>
                    @foreach($creches as $id => $creches)
                        <option value="{{ $id }}" {{ in_array($id, old('creches', [])) ? 'selected' : '' }}>{{ $creches }}</option>
                    @endforeach
                </select>
                @if($errors->has('creches'))
                    <span class="text-danger">{{ $errors->first('creches') }}</span>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div>
@endsection