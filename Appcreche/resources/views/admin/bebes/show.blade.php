@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('bébé') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bebes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <a href="{{ route('admin.informations_supplementaires.create') }}">
                    <i style="color:orange;" class="fas fa-plus-circle"></i>
                    
                    {{ trans('Ajouter des informations supplémentaires') }}
                </a>

            </div>
            <table class="" >
                <tbody >
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Id') }}
                        </th>
                        <td>
                            {{ $bebe->id }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Situation familiale') }}
                        </th>
                        <td>
                            {{ $bebe->nom }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Responsable légal(e)') }}
                        </th>
                        <td>
                            {{ $bebe->prenom }}
                        </td>
                    </tr>
                    <br>
                    <tr>
                        <th><br><i> Personne à prévenir en cas d'urgence</i></th>
                    </tr>     
                    <tr>      
                        <th style="font-weight:normal;">
                            {{ trans('Nom') }}
                        </th>
                        <td>
                            {{ $bebe->date_naissance }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Prénom') }}
                        </th>
                        <td>
                            {{ $bebe->sexe }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Téléphone') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Lien') }}
                        </th>
                        <td>
                            <span class="badge badge-info">{{ $bebe->creche->nom }}</span>
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('CNI') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th><br><i> Pédiatre de l'enfant</i></th>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Nom') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Prénom') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Téléphone') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th><br><i> Personne autorisée à prendre l'enfant &nbsp;&nbsp;</i></th>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Nom') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Prénom') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                    <tr>
                        <th style="font-weight:normal;">
                            {{ trans('Téléphone') }}
                        </th>
                        <td>
                            {{ $bebe->nationalite }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bebes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection