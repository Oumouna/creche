@extends('layouts.admin')
@section('content')
@can('creche_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.creches.create") }}">
                {{ trans('global.add') }} {{ trans('Crèche') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('crèche') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-creche">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('id') }}
                        </th>
                        <th>
                            {{ trans('nom') }}
                        </th>
                        <th>
                            {{ trans('adresse') }}
                        </th>
                        <th>
                            {{ trans('telephone') }}
                        </th>
                        <th>
                            {{ trans('directeur') }}
                        </th>
                        <th>
                            {{ trans('logo') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($creches as $key => $creche)
                        <tr data-entry-id="{{ $creche->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $creche->id ?? '' }}
                            </td>
                            <td>
                                {{ $creche->nom ?? '' }}
                            </td>
                            <td>
                                {{ $creche->adresse ?? '' }}
                            </td>
                            <td>
                                {{ $creche->telephone ?? '' }}
                            </td>
                            <td>
                                {{ $creche->nom_directeur ?? '' }}
                            </td>
                            <td>
                                <img class="card-img-top" src="{{ asset('logos/thumbnail/'.$creche->thumbnail_img)}} ">

                            </td>
                            <td>
                                @can('creche_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.creches.show', $creche->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('creche_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.creches.edit', $creche->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('creche_delete')
                                    <form action="{{ route('admin.creches.destroy', $creche->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection

