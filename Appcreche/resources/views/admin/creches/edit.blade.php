@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('crèche') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.creches.update", [$creche->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="nom">{{ trans('Nom') }}</label>
                <input class="form-control {{ $errors->has('nom') ? 'is-invalid' : '' }}" type="text" name="nom" id="nom" value="{{ old('nom', $creche->nom) }}" required>
                @if($errors->has('nom'))
                    <span class="text-danger">{{ $errors->first('nom') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="adresse">{{ trans('Adresse') }}</label>
                <input class="form-control {{ $errors->has('adresse') ? 'is-invalid' : '' }}" type="text" name="adresse" id="adresse" value="{{ old('adresse', $creche->adresse) }}" required>
                @if($errors->has('adresse'))
                    <span class="text-danger">{{ $errors->first('adresse') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="telephone">{{ trans('Téléphone') }}</label>
                <input class="form-control {{ $errors->has('telephone') ? 'is-invalid' : '' }}" type="number" name="telephone" id="telephone" value="{{ old('telephone', $creche->telephone) }}" required>
                @if($errors->has('telephone'))
                    <span class="text-danger">{{ $errors->first('telephone') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="nom_directeur">{{ trans('Prénom et nom du directeur') }}</label>
                <input class="form-control {{ $errors->has('nom_directeur') ? 'is-invalid' : '' }}" type="text" name="nom_directeur" id="nom_directeur" value="{{ old('nom_directeur', $creche->nom_directeur) }}" required>
                @if($errors->has('nom_directeur'))
                    <span class="text-danger">{{ $errors->first('nom_directeur') }}</span>
                @endif
                <span class="help-block">{{ trans('') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="thumbnail_img">{{ trans('Logo') }}</label>
                <input class="form-control {{ $errors->has('thumbnail_img') ? 'is-invalid' : '' }}" type="file" name="thumbnail_img" id="thumbnail_img" required>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if($errors->has('logo'))
                    <span class="text-danger">{{ $errors->first('logo') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div>
@endsection