@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('Crèche') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.creches.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>     
                    <tr>
                        <th>
                            {{ trans('Id') }}
                        </th>
                        <td>
                            {{ $creche->id }}
                        </td> 
                    </tr>
                 
                    <tr>
                        <th>
                            {{ trans('Nom') }}
                        </th>
                        <td>
                            {{ $creche->nom }}
                        </td>
                    </tr>
                   
                    <tr>
                        <th>
                            {{ trans('Adresse') }}
                        </th>
                        <td>
                            {{ $creche->adresse }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Télépone') }}
                        </th>
                        <td>
                            {{ $creche->telephone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Directeur (trice)') }}
                        </th>
                        <td>
                            {{ $creche->nom_directeur }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Logo') }}
                        </th>
                        <td>
                            <img class="card-img-top" src="{{ asset('logos/thumbnail/'.$creche->thumbnail_img)}} ">
                        </td>
                    </tr>
                    
                </tbody>
                
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.creches.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection