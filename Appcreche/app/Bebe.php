<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bebe extends Model
{
    protected $table = 'bebe';

    protected $fillable = array('nom', 'prenom', 'date_naissance', 'sexe', 'nationalite', 'creche_id');

    public static $rules = array('nom'=>'nullable|min=1',
                                 'prenom'=>'nullable|min=1',
                                 'date_naissance'=>'numeric',
                                 'sexe'=>'nullable|min=1',
                                 'nationalite'=>'nullable',
                                 'creche_id'=>'nullable|integer'    
                                );


    public function creche()
    {
        return $this->belongsTo(Creche::class, 'creche_id');
    }

    public function parents()
    {
        return $this->belongsToMany('Parents');
    }

    public function journee_bebe()
    {
        return $this->hasMany('Journee_Bebe');
    }

    public function informations_supplementaires()
    {
        return $this->hasOne('Informations_Supplementaires');
    }

    public function prestation()
    {
        return $this->belongsToMany('Prestation');
    }

    public function sante()
    {
        return $this->hasOne('Sante');
    }

    public function facturation()
    {
        return $this->hasManyThrough('Facturation');
    }
}
