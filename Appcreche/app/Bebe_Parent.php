<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bebe_Parent extends Model
{
    protected $table = 'bebe_parent';
    public $timestamps = true;

}
