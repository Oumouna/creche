<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bebe_Prestation extends Model
{
    protected $table = 'bebe_prestation';
    public $timestamps = true;
}
