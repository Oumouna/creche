<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sante extends Model
{
    protected $table = 'sante';
    public $timestamps = true;

    public function bebe()
    {
        return $this->belongsTo('Bebe');
    }

}
