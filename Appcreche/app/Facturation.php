<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturation extends Model
{
    protected $table = 'facturation';
    public $timestamps = true;

    public function prestation()
    {
        return $this->belongsTo('Prestation');
    }
}
