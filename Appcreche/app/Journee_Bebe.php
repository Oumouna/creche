<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journee_Bebe extends Model
{
    protected $table = 'journee_bebe';
    public $timestamps = true;

    public function bebe()
    {
        return $this->belongsTo('Bebe');
    }

}
