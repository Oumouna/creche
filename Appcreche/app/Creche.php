<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creche extends Model
{
    protected $fillable = array('nom', 'adresse', 'telephone', 'nom_directeur', 'thumbnail_img');

    public static $rules = array('nom'=>'nullable|min=1',
                                 'adresse'=>'nullable|min=1',
                                 'telephone'=>'numeric',
                                 'nom_directeur'=>'nullable|min=1',
                                 'thumbnail_img'=>'nullable'    
                                );

    
    public function users()
    {
    return $this->belongsToMany(User::class);
    }


    public function bebe()
    {
        return $this->hasMany(Bebe::class);
    }
    
}
