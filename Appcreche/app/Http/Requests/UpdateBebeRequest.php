<?php

namespace App\Http\Requests;

use App\Bebe;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBebeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom'    => [
                'required',
            ],
            'prenom'   => [
                'required',
            ],
            'date_naissance' => [
                'date',
            ],
            'sexe'   => [
                'required',
            ],
            'nationalite'   => [
                'required',
            ],
            'creche_id'   => [
                'required',
            ],
        ];
    }
}
