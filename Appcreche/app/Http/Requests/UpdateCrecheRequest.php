<?php

namespace App\Http\Requests;

use App\Creche;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCrecheRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom'         => [
                'required',
            ],
            'adresse'   => [
                'required',
            ],
            'telephone'   => [
                'required',
            ],
            'nom_directeur'   => [
                'required',
            ],
            'thumbnail_img'   => [
                'required',
            ],
        ];
    }
}
