<?php

namespace App\Http\Requests;

use App\Bebe;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreBebeRequest extends FormRequest
{
    public function authorize()
    {

        return true;
    }

    public function rules()
    {
        return [
            'nom'     => [
                'required',
            ],
            'prenom'    => [
                'required',
            ],
            'date_naissance' => [
                'required',
            ],
            'sexe'  => [
                'required',
            ],
            'nationalite'    => [
                'required',
            ],
            'creche_id'    => [
                'nullable',
            ],
        ];
    }
}
