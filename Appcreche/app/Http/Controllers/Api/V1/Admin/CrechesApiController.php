<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCrecheRequest;
use App\Http\Resources\Admin\CrecheResource;
use App\Creche;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CrechesApiController extends Controller
{
    public function index()
    {

        return new CrecheResource(Creche::with(['roles'])->get());
    }

    public function store(StoreCrecheRequest $request)
    {
        $creche = Creche::create($request->all());
        $creche->roles()->sync($request->input('roles', []));

        return (new CrecheResource($creche))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Creche $creche)
    {
        return new CrecheResource($creche);
    }

    public function update(UpdateCrecheRequest $request, Creche $creche)
    {
        $creche->update($request->all());

        return (new CrecheResource($creche))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Creche $creche)
    {
        abort_if(Gate::denies('creche_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $creche->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
}
