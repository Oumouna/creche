<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use File;
use ImageResize;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCrecheRequest;
use App\Http\Requests\UpdateCrecheRequest;
use App\Http\Requests\MassDestroyCrecheRequest;

use App\Creche;
use Gate;


use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CrechesController extends Controller
{
    public function index()
    {
        
        $creches = Creche::all();

        return view('admin.creches.index', compact('creches'));
    }
    
    public function create()
    {

        $creches = Creche::all();

        return view('admin.creches.create', compact('creches'));
    }

    public function store(StoreCrecheRequest $request)
    { 

        $this->validate($request, [
            'nom' => 'required',
            'adresse' => 'required',
            'telephone' => 'required',
            'nom_directeur' => 'required',
            'thumbnail_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);
        
        $image = $request->file('thumbnail_img');
        $input['imagename'] = time().'.'.$image->extension();
        $destinationPath = public_path('/logos/thumbnail');
        $img = ImageResize::make($image->path());


        // --------- [ Resize Image ] ---------------

        $img->resize(100, 50, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);


        // ----------- [ Uploads Image in Original Form ] ----------
        
        $destinationPath = public_path('/logos/original');

        $image->move($destinationPath, $input['imagename']);

        // store into database table

        Creche::create([
            'nom' => request()->get('nom'),
            'adresse' => request()->get('adresse'),
            'telephone' => request()->get('telephone'),
            'nom_directeur' => request()->get('nom_directeur'),
            'img' => $input['imagename'], 'thumbnail_img' => $input['imagename']

        
            ]); 

            
        return redirect()->route('admin.creches.index');
    }


    public function edit($id)
    {
        $creche = Creche::where("id", "=", $id)->firstOrFail();
        return view('admin.creches.edit', compact('creche'));
    }

    public function update(Request $request, $id )
    {
        $request->validate([
            'nom' => 'required',
            'adresse' => 'required',
            'telephone' => 'required',
            'nom_directeur' => 'required',
            'thumbnail_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            
        ]);
    
        $creche = Creche::findOrFail($id);
        $creche->update($request->all());

        if ($request->hasfile('thumbnail_img')){
            $image = $request->file('thumbnail_img');
        $input['imagename'] = time().'.'.$image->extension();
        $destinationPath = public_path('/logos/thumbnail');
        $img = ImageResize::make($image->path());

        $img->resize(100, 50, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);

        $destinationPath = public_path('/logos/original');

        $image->move($destinationPath, $input['imagename']);

        $creche->update();
     
        }
        

        

        return redirect()->route('admin.creches.index');
        
    }

    public function show($id)
    {
        $creche = Creche::where("id", "=", $id)->firstOrFail();
        return view('admin.creches.show', compact('creche'));
    }

    public function destroy($id)
    {
        $creche = Creche::find($id);
        if( $creche != null)
        {
            $creche->delete();
        }
        return back();
    }

    public function massDestroy(MassDestroyCrecheRequest $request)
    {
        Creche::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
