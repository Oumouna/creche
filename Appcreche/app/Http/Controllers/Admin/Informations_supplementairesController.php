<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Informations_Supplementaires;

class Informations_supplementairesController extends Controller
{
    public function index()
    {
        
        $informations_supplementaires = Informations_supplementaires::all();

        return view('admin.informations_supplementaires.index', compact('informations_supplementaires'));
    }

    public function create()
    {

        $informations_supplementaires = Informations_supplementaires::all();

        return view('admin.informations_supplementaires.create', compact('informations_supplementaires'));
    }
}
