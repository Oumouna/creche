<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBebeRequest;
use App\Http\Requests\UpdateBebeRequest;
use App\Http\Requests\MassDestroyBebeRequest;


use Gate;
use Auth;
use App\Bebe;
use App\Creche;
use App\User;
use App\Role;
use Symfony\Component\HttpFoundation\Response;
 
class BebesController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        $user->load('creches');

        if ($user->roles()->where('title', 'admin')->exists()) {
            $bebes = Bebe::whereHas('creche', function($query) use ($user) { 
                $query->where('nom', $user->creches->first()->nom); 
            })->get();
        } else {
            $bebes = Bebe::all();
        } 
           
        return view('admin.bebes.index', compact('bebes','user'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();

        if ($user->roles()->where('title', 'admin')->exists()) {
            $creches = Creche::where('nom', $user->creches->first()->nom)->pluck('nom', 'id');
        } else {
            $creches = Creche::all()->pluck('nom', 'id');
        } 

        return view('admin.bebes.create', compact('creches'));
    }

    public function store(StoreBebeRequest $request)
    {
        $bebe = Bebe::create($request->all());

        return redirect()->route('admin.bebes.index');
    }

    public function edit(Bebe $bebe)
    {
        $user = Auth::user();

        if ($user->roles()->where('title', 'admin')->exists()) {
            $creches = Creche::where('nom', $user->creches->first()->nom)->pluck('nom', 'id');
        } else {
            $creches = Creche::all()->pluck('nom', 'id');
        } 
 
        return view('admin.bebes.edit', compact('bebe','creches'));
    }

    public function update(UpdateBebeRequest $request, Bebe $bebe)
    {
        $bebe->update($request->all());

        return redirect()->route('admin.bebes.index');
    }

    public function show(Bebe $bebe)
    {
        return view('admin.bebes.show', compact('bebe'));
    }

    public function destroy(Bebe $bebe)
    {
        ('bebe_delete');

        $bebe->delete();

        return back();
    }

    public function massDestroy(MassDestroyBebeRequest $request)
    {
        Bebe::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
