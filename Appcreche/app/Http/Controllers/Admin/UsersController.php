<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\Creche;
use App\User;
use Gate;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        
        $user = Auth::user();
        $user->load('creches');

        if ($user->roles()->where('title', 'admin')->exists()) {
            $users = User::whereHas('roles', function($query) { 
                $query->where('title', '<>', 'superAdmin'); 
            })->whereHas('creches', function ($query) use ($user) {
                $query->where('nom', $user->creches->first()->nom);
            })->get();
        } else {
            $users = User::all();
        } 
           
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();

        if ($user->roles()->where('title', 'admin')->exists()) {
            $roles = Role::where('title', '<>', 'superAdmin')->pluck('title', 'id');
            $creches = Creche::where('nom', $user->creches->first()->nom)->pluck('nom', 'id');
        } else {
            $roles = Role::all()->pluck('title', 'id');
            $creches = Creche::all()->pluck('nom', 'id');
        } 

        return view('admin.users.create', compact('roles', 'creches'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->creches()->sync($request->input('creches', []));

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();

        if ($user->roles()->where('title', 'admin')->exists()) {
            $roles = Role::where('title', '<>', 'superAdmin')->pluck('title', 'id');
            $creches = Creche::where('nom', $user->creches->first()->nom)->pluck('nom', 'id');
        } else {
            $roles = Role::all()->pluck('title', 'id');
            $creches = Creche::all()->pluck('nom', 'id');
        }

        $user->load('roles');
        $user->load('creches');

        return view('admin.users.edit', compact('roles', 'creches', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->creches()->sync($request->input('creches', []));

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->load('roles');
        $user->load('creches');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
