<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informations_Supplementaires extends Model
{
    protected $table = 'informations_supplementaires';
    public $timestamps = true;

    public function bebe()
    {
        return $this->belongsTo('Bebe');
    }
}
