<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $table = 'prestation';
    public $timestamps = true;

    public function bebe()
    {
        return $this->belongsToMany('Bebe');
    }

    public function facture()
    {
        return $this->hasMany('Facturation');
    }
}
