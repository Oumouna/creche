<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    protected $table = 'parent';
    public $timestamps = true;

    public function bebe()
    {
        return $this->belongsToMany('Bebe');
    }
}
